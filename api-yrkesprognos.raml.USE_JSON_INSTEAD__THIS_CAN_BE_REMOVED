#%RAML 1.0
title: api-yrkesprognos
version: v1

annotationTypes:
  dcat-dataset:
    properties:
      about: string
      title-sv: string
      description-sv: string
      creator:
        required: false
        properties:
          about: string
          name: string
          type: string
          homepage: string
          mbox: string
      other agent:
        required: false
        properties:
          about: string
          name: string
          type: string
          homepage: string
          mbox: string
          role: string
      contactPoint:
        properties:
          about: string
          type: string
          name: string
          email: string
          phone: 
            type: string
            required: false
          address: string
      keyword-sv: string
      theme: string
      identifier: 
        type: string
        required: false
      adms: 
        type: string
        required: false
      issued: 
        type: string
        required: false
      modified: 
        type: string
        required: false
      landingPage: string
      temporalResolution: 
        type: string
        required: false
      spatialResolutionInMeters: 
        type: string
        required: false
      versionInfo: 
        type: string
        required: false
      versionNotes: 
        type: string
        required: false
      accessRights: string
      isReferencedBy: 
        type: string
        required: false
      relation: 
        type: string
        required: false
      spatialUrl: string
      conformsTo:
        required: false
        properties:
          about: string
          title-sv: string
          description-sv: string
      offers:
        required: false
        properties:
          description-sv: string
          page: 
            type: string
            required: false
      page:
        required: false
        properties:
          about: string
          title-sv: string
          description-sv: string
      temporal:
        required: false
        properties:
          startDate: string
          endDate: string
      distribution:
        properties:
          about: string
          title-sv: string
          description-sv: string
          accessURL: string
          downloadURL: string
          formatMedia: 
            type: string 
            required: false
          accessService: 
            type: string
            required: false
          temporalResolution: 
            type: string
            required: false
          spatialResolutionInMeters: 
            type: string
            required: false
          byteSize: 
            type: integer
            required: false
          issued: 
            type: string
            required: false
          modified: 
            type: string
            required: false
          status: string
          availability: string
          licenseurl: string
          rights:
            required: false
            properties:
              attributionText-sv: string
              attributionURL: string
              copyrightNotice: string
              copyrightStatement:
                properties:
                  about: string
                  title-sv: string
                  description-sv: string
              copyrightYear: integer
              copyrightHolder:
                properties:
                  about: string
                  name: string
                  type: string
                  homepage: string
                  mbox: string
              jurisdiction: string
              reuserGuidelines: string
          checksum:
            required: false
            properties:
              value: string
          page:
            required: false
            properties:
              about: string
              title-sv: string
              title-en: string
              description-sv: string
              description-en: string
          conformsTo:
            required: false
            properties:
              about: string
              title-sv: string
              description-sv: string
  dcat-dataservice:
    properties:
      about: string
      title-sv: string
      description-sv: string
      endpointURL: string
      endpointDescription: string
      publisher:
        properties:
          about: string
          name: string
          type: string
          homepage: string
          mbox: string
      contactPoint:
        properties:
          about: string
          type: string
          name: string
          email: string
          phone: string
          address: string
      type: string
      keyword-sv: string
      theme: string
      conformsTo:
        required: false
        properties:
          about: string
          title-sv: string
          description-sv: string
      servesDataset: string
      licensedoc:
        properties:
          about: string
          title-sv: string
          description-sv: string
      accessRights: string
      landingPage: string
      page:
        properties:
          about: string
          title-sv: string
          description-sv: string

(dcat-dataset): 
  about: https://arbetsformedlingen.se/#datasetYrkesprognoser
  title-sv: Yrkesprognos 
  title-en: Occupational forecast
  description-en: The short term and long term forecasts are developed by the Department of Analysis.The model is based on register data from SCB on occupational codes (SSYK2012), labour market status, education, age and statistics on job openings retrieved from the Swedish Public Employment Office. The model accounts for population forecast and assessment of the business cycle on industry level. The forecast is reported for about 180 occupations. One occupation consists of one or several SSYK codes.
  description-sv: Bedömningarna på lång och kort sikt baseras på en modell byggd av analysavdelningen med registerdata från SCB på Standard för svensk yrkesklassificering (SSYK2012), sysselsättningsstatus, utbildning samt ålder samt Arbetsförmedlingens verksamhetsstatistik på platsannonser. Prognosen justeras med hjälp av SCB:s befolkningsprognoser samt konjunkturbedömningar för näringsgrenarna. Prognosen redovisas för ca 180 prognosyrken, där ett prognosyrke består av en eller flera SSYK-yrkesgruppe  
  contactPoint:
    about: https://arbetsformedlingen.se/#contactYrkesprognoser
    name: Arbetsförmedlingen
    type: Organization
    email: jobtechdev@arbetsformedlingen.se
    address: Elektrogatan 4; 17154; Solna; Sverige    
  keyword-sv: arbetsmarknad; yrkesprognos
  keyword-en: labour market; occupational forecast
  theme: GOVE 
  spatialUrl: https://www.geonames.org/2661886/kingdom-of-sweden.html  
  landingPage: https://jobtechdev.se/sv/komponenter/yrkesprognoser   
  accessRights: Public 
  page:    
    about: https://jobtechdev.se/sv/komponenter/yrkesprognoser/metodbeskrivning-yrkesprognoser/#distributionYrkesprognoser/page
    title-sv: Metodbeskrivning yrkesprognoser
    title-en: Method description  
    description-en: Description of method to produce occupational forecasts.
    description-sv: Beskrivning av metoden för att ta fram yrkeprognoser.
  distribution:
    about: https://arbetsformedlingen.se/#distributionYrkesprognoser
    title-sv: Yrkesprognoser
    tilte-en: Occupational forecast
    description-sv: Bedömningarna på lång och kort sikt baseras på en modell byggd av analysavdelningen med registerdata från SCB på Standard för svensk yrkesklassificering (SSYK2012)
    description-en: The short term and long term forecasts are developed by the Department of Analysis.The model is based on register data from SCB on occupational codes (SSYK2012)
    accessURL: https://apier.arbetsformedlingen.se/api-yrkesprognos/v1/yrkesprognos  
    downloadURL: https://data.jobtechdev.se/yrkesprognoser/yrkesprognoser2021/YP_med_texter.csv     
    status: Completed
    availability: Stable
    licenseurl: CC0 1.0          
    conformsTo:
      about: https://apier.arbetsformedlingen.se/api-yrkesprognos/v1/yrkesprognos
      title-sv: Swagger gränsnitt
      description-sv: Swagger gränsnitt för Yrkesprognoser
    
    

uses:
  ipf: exchange_modules/1dae6e32-bf25-4b52-aa03-8f32e1f5fc08/ipf-api-common/0.2.32/ipf.raml
  errorType: types/errorTypes.raml
  responseType: types/responseType.raml

/yrkesprognos:
  get:
    responses:
      200:
        description: Ok
        body:
          application/json:
            type: responseType.yrkesprognosDTO
            example: !include examples/yrkesprognos.json
      400:
        description: Validation Error
        body:
          application/json:
            type: errorType.errorDTO
            example: !include examples/error-response-example.json
      401:
        description: Unauthorized
        body:
          application/json:
            type: errorType.errorDTO
            example: !include examples/error-response-example.json
      422:
        description: Malformed
        body:
          application/json:
            type: errorType.errorDTO
            example: !include examples/error-response-example.json